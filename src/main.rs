#[macro_use]
extern crate clap;
extern crate colored;
extern crate tempfile;

mod utils;
mod special;

#[derive(Clap)]
#[clap(
    version = "1.1",
    author = "Qingluan",
    about = r#"
    gui builder
    default tui
    "#
)]
struct Opts{
    #[clap(short="i", long="init", default_value="")]
    name:String
}

fn main() -> utils::IoOut<()>{
    utils::log_inif();
    let opts = Opts::parse();
    if opts.name.len()> 0{
        let r = utils::create_project(&opts.name)?;
        utils::create_template(std::path::Path::new(&r))?;
        Ok(())
    }else{
        Ok(())
    }

}
